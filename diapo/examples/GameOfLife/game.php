<?php
namespace GameOfLife;

class GameOfLifeRules
{
    private $board;
    public function __construct($board)
    {
        $this->board = $board;
    }
    public static function initializeGame($nbOfRow, $nbOfCol)
    {
        $game = [];
        for ($i = 0; $i < $nbOfRow; $i++) {
            $game[] = [];
            for ($j = 0; $j < $nbOfCol; $j++) {
                $game[$i][$j] = (bool)random_int(0, 1);
            }
        }
        return new GameOfLifeRules($game);
    }

    /**
     * Count alive neighboors for the cell at the given coordinate
     * @param $row int the row index
     * @param $col int the column index
     * @return int the number of alive neighboors for the wanted cell
     */
    public function countNeightboors($row, $col)
    {
        $neigboorsCoordinates = [[$row - 1, $col - 1], [$row - 1, $col], [$row - 1, $col + 1],
            [$row, $col - 1], [$row, $col + 1],
            [$row + 1, $col - 1], [$row + 1, $col], [$row + 1, $col + 1]];
        $neighboors = 0;
        foreach ($neigboorsCoordinates as $coordinates) {
            if (isset($this->board[$coordinates[0]]) && isset($this->board[$coordinates[0]][$coordinates[1]])) {
                $neighboors += $this->board[$coordinates[0]][$coordinates[1]];
            }
        }
        return $neighboors;
    }


    public function birth($array, $row, $col)
    {
        return (int)(countNeightboors($array, $row, $col) == 3);
    }

    public function death($array, $row, $col)
    {
        return (int)!(countNeightboors($array, $row, $col) >= 4 || countNeightboors($array, $row, $col) <= 1);
    }

    public function nextGeneration($oldGeneration)
    {
        $newGeneration = initializeGame(count($oldGeneration), count($oldGeneration[0]));
        foreach ($oldGeneration as $rowIndex => $row) {
            foreach ($row as $colIndex => $value) {
                $newGeneration[$rowIndex][$colIndex] = $value ? death($oldGeneration, $rowIndex, $colIndex) : birth($oldGeneration, $rowIndex, $colIndex);
            }
        }
        return $newGeneration;
    }

    public function gameIsStill($new)
    {
        foreach ($this->board as $rowIndex => $row) {
            if (is_array($row) && !gameIsStill($row, $new->board[$rowIndex])) {
                return false;
            } elseif (!is_array($row) && $row != $new->board[$rowIndex]) {
                return false;
            }
        }
        return true;
    }
}
