<?php
namespace GameOfLife\tests\units;
require dirname(__FILE__) . '/../../game.php';
use GameOfLife;
use mageekguy\atoum;

class GameOfLifeRules extends atoum\test
{
    public function testNeighboorInCorner() {
        $this->given($game = new GameOfLife\GameOfLifeRules([
            [0, 1, 1],
            [0, 1, 1],
            [1, 0, 1],
        ]))
        -> then
            ->integer($game->countNeightboors(0, 0))
            ->isEqualTo(2);

    }
    public function testNeighboorInMiddle() {
        $this->given($game = new GameOfLife\GameOfLifeRules([
            [0, 1, 1],
            [0, 1, 1],
            [1, 0, 1],
        ]))
            -> then
            ->integer($game->countNeightboors(1, 1))
            ->isEqualTo(5);

    }
}