<!DOCTYPE HTML>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Formulaire épicerie</title>
</head>
<body>
    <form action="liste_produits.php" method="POST">
        <p><label>Entrez le nom du produit à ajouter <input type="text" name="nomproduit" required/></label></p>
        <p><label>A quel prix est il vendu ? <input type="number" name="prix" required step="0.10"/></label></p>
        <p><label>Combien en stockez vous ? <input type="number" name="quantité" required step="1"/></label></p>
        <p><label>A quoi ressemble ce produit ? <input type="file" name ="img" required/></label>
        <p><input type="submit"/><p>
    </form>
</body>

<?php
    session_start();
    $i  = $_SESSION['photoNum'] + 1;
    $upimg = move_uploaded_file($_FILES ['img']["tmp_name"],"Image/Photo_".$i);
    $path = "liste_pdts.csv";
    $between = ";";
    
    $open = fopen($path, 'r+');
    
    fputcsv($open, $ligne, $between);
    
    $_SESSION['quantité'] = $_POST['quantité'] ;
    $_SESSION['prix'] = $_POST['prix'] ;
    $_SESSION['nomproduit'] = $_POST['nomproduit'] ;
    
?>
    
</html>
