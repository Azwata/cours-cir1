<?php

include 'functionformydb.php';

class Events{
    
    public function eventsBetween($start, $end){
        $pdo = linkToDb();
        $sql = "SELECT * FROM events WHERE startdate BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}'";
        $statement = $pdo->query($sql);
        $results = $statement->fetchAll();
        return$results;
    }
    
    public function eventBetweenDay($start,$end){
        $events = $this->eventsBetween($start, $end);
        $days = [];
        foreach ($events as $event){
            $date = explode(' ',$event['startdate'])[0];
            if(!isset($days[$date])){
                $days[$date] = [$event];
            }
            else{
                $days[$date][] = [$event];
            }
        }
        return $days;
    }
    
    public function getId($id){
        $pdo = linkToDb();
        return $pdo->query("SELECT * FROM events WHERE id = $id ")->fetch();
    }
    
}
