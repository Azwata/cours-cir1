<?php



class Month {
    public $days = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'];
    private $months =['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre'] ;
    public $month;
    public $year;
    public function __construct($month = null, $year = null){
        if($month === null || $month < 1 || $month > 12){
            $month = date('m');
        }
        if($year === null){
            $year = date('Y');
        }
        $this->month = $month;
        $this->year = $year;
    }
    public function fullLetters(){
        return $this->months[$this->month - 1].' '.$this->year;
    }
    public function firstDay(){
        return new DateTime("{$this->year}-{$this->month}-01");
    }
    
    public function numWeeks(){
        
        $first = $this->firstDay();
        $last = (clone $first)->modify('+1 month -1 day');
        $wNum = $last->format('W')-$first->format('W') + 1;
        if($wNum < 0){
            $wNum = $last->format('W');
        }
        if($wNum == 1){
            $wNum = 6;
        }
        return $wNum;
    }
    
    public function inMonth($date){
        return $this->firstDay()->format('Y-m') === $date->format('Y-m');
    }
    
    public function prevMonth(){
        $month = $this->month - 1;
        $year = $this->year;
        if($month < 1){
            $month = 12;
            $year -= 1;
        }
        return new Month($month, $year);
    }
    
    public function nextMonth(){
        $month = $this->month + 1;
        $year = $this->year;
        if($month > 12){
            $month = 1;
            $year += 1;
        }
        return new Month($month, $year);
    }
    
    
}