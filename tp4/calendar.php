<?php
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Shared Calendar</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <a href="/calendar.php" class="navbar-brand">Calendrier evenementiel</a>
        </nav>
        
        <?php 
        require 'Month.php';
        require 'Events.php';
        $event = new Events();
        $month = new Month($_GET['month'] ?? null, $_GET['year']); 
        $weeks = $month->numWeeks();
        $first = $month->firstDay();
        $first = $first->format('N') === '1' ? $first : $month->firstDay()->modify('last monday');
        $end = (clone $first)->modify('+'.(6 + 7 * ($weeks - 1 )).'days');
        $event = $event->eventBetweenDay($first, $end);
        ?>
        <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
            <a href="/calendar.php?month=<?= $month->prevMonth()->month;?>&year=<?= $month->prevMonth()->year;?>" class="btn btn-outline-danger">&ltcc;</a>
            <h1><?= $month->fullLetters(); ?></h1>
            <a href="/calendar.php?month=<?= $month->nextMonth()->month;?>&year=<?=$month->nextMonth()->year;?>" class="btn btn-outline-danger">&gtcc;</a>
        </div>
        <table class="days">
            <tr>
                <td>Lundi</td>
                <td>Mardi</td>
                <td>Mercredi</td>
                <td>Jeudi</td>
                <td>Vendredi</td>
                <td>Samedi</td>
                <td>Dimanche</td>
            </tr>
        </table>
        <table class="calTable">
            <?php for($i = 0; $i< $weeks; $i++):?>
            <tr>
                <?php foreach($month->days as $j => $day):
                    $date = (clone $first)->modify("+".($j + $i * 7)." days");
                    $eventThisDay = $event[$date->format('Y-m-d')] ?? [];
                    ?>                
                <td class="<?= $month->inMonth($date) ? '' : 'calPrevMonth';?>">                   
                    <?= $date->format('d');?>
                    <?php foreach ($eventThisDay as $events): ?>
                    <div class="event">
                        <?= (new DateTime($events['startdate']))->format('H:i');?> - <a href="event.php?id=<?=$events['id'];?>"><?= $events['name'];?></a>
                    </div>
                    <?php endforeach;?>
                </td>
                <?php endforeach; ?>
                
            </tr>
                
            <?php endfor;?>
        </table>
        <div class="addBtn">
            <a href="/addevent.php"  class="btn btn-outline-danger plusBtn">+</a>
        </div>
            
        
    </body>
</html>