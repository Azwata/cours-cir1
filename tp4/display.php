<?php

?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Shared Calendar</title>
        <link href="bootstrap.css" rel="stylesheet">
    </head>
    <body>
        <h1 class="text-center">
            Event Calendar
            <small class="h6 text-muted">by Baptiste Royer</small>
        </h1>
        <form method="post" action="./login.php" enctype="multipart/form-data">
          <div class="form-group">
            <label for="logAccount">Account name</label>
            <input type="text" class="form-control" id="id_logAccount" placeholder="Pseudo" name="logAccount">
          </div>
          <div class="form-group">
            <label for="logPassword">Password</label>
            <input type="password" class="form-control" id="log_logPassword" placeholder="Password" name="logPassword">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </body>
</html>
    

