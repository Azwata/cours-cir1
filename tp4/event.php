<?php
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Shared Calendar</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="calendar.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <a href="/calendar.php" class="navbar-brand">Calendrier evenementiel</a>
        </nav>
        
        <?php 
        require 'Events.php';
        $events = new Events();
        $event = $events->getId($_GET['id'] ?? null);
        ?>
        
        <h1><?= htmlentities($event['name']);?></h1>
        <ul>
            <li>Date : <?=(new DateTime($event['startdate']))->format('d/m/Y');?></li>
            <li>Commence à : <?=(new DateTime($event['startdate']))->format('H:i');?> </li>
            <li>Termine à : <?=(new DateTime($event['enddate']))->format('H:i');?> </li>
            <li>Description : <br>
                <?= htmlentities($event['description'])?></li>
            <li>Nombre de places : <?=$event['nb_place']?></li>
        </ul>

        
    </body>
</html>