<?php

function linkToDb(){
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $db = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', '270314', $opts);
    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.');
    }
    return $db;
}