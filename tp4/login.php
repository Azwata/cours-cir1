<?php
    session_start();
    include('functionformydb.php');
    $db = linkToDb();
    $database = $db->prepare('SELECT password,id,rank FROM Users WHERE login = ?');
    $database->execute(array($_POST['logAccount']));
    $result=$database->fetch();
    $correctpass = password_verify($_POST['logPassword'],$result['password']);
    if($correctpass){
        $_SESSION['id_user'] = $result['id'];
        $_SESSION['user_name'] = $_POST['logAccount'];
        $_SESSION['rank_user'] = $result['rank'];
        header('Location: ./calendar.php');
        exit();
    }